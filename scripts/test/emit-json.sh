#!/bin/bash

# This script tests only the lua workflow

# Given a name from 0-config-lua, it runs the lua pipeline from 0 to 3, and
# outputs the resulting .lncfs.json file to a temporary file output.
# For example, passing in "example" to the bash script will load
# 0-config-lua/example.lncf.lua

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

file=$1
if [ $# -eq 0 ]
  then
    file="example"
fi

top_level="$(realpath "$parent_path/../../")"
cd "$top_level/src/lua"
# printf "$top_level/examples/$file.lncf.lua\n"
cat "$top_level/examples/$file.lncf.lua" | ./lualncf