#!/bin/bash

# Runs emit-json.sh but gives the output to jq

parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

IN="$($parent_path/emit-json.sh $1)"

which jq > /dev/null
if [ $? -eq 0 ]
then
  if [ -t 1 ]
  then
    echo ${IN} | jq -C -S .
  else 
    echo ${IN} | jq -M -S .
  fi
  exit 0
fi

exit 1