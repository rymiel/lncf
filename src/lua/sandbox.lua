local _program,_errormessage,_outputcode,_output
_program=io.read"*a"

local write=io.write
local flush=io.flush
local json = require 'vendor.json'
local lncf = require "liblncf"
local flows = lncf.flows
local key = lncf.key

local pressmt = setmetatable
function secsmt(givenkey)
	if (givenkey == key) then
		return pressmt
	else
		return nil
	end
end

for k,v in pairs(lncf) do _G[k] = v end

debug.debug=nil
debug.getregistry=nil
dofile=nil
io=nil
loadfile=nil
setmetatable=nil
getmetatable=nil
os.execute=nil
os.getenv=nil
os.remove=nil
os.rename=nil
os.tmpname=nil
package.loaded.io=nil
package.loaded.package=nil
package=nil
require=nil

_output,_errormessage=load(_program,"=input")
_outputcode="ERROR"
if not _output then
	_errormessage="failed to compile " .. _errormessage
else
	_output=(function (...) return {select('#',...),...} end)(pcall(_output))
	if not _output[2] then
		_errormessage="failed to run (" .. _output[3] .. ")"
	else
		_errormessage="ran successfully"
		_outputcode="OK"
	end
end
write(json.encode({
    -- source=_program, -- probably not needed
    status=_errormessage,
	output={
		flows=flows
	},
	code=_outputcode
}) .. "\n")
flush()
