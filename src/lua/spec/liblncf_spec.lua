L = require 'liblncf'

describe("liblncf", function()
    -- Metatable security isn't necessary in this test file, but liblncf still
    -- expects secsmt as a function to access setmetatable
    function _G.secsmt(_)
        return setmetatable
    end

    describe("fn calls", function()
        it("parses argless local call", function()
            local expected = {["$"] = "fn", fn = {"local", "test"}}
            local actual = L.fn "test"
            assert.are.same(expected, actual)
        end)
        it("parses argless library call", function()
            local expected = {["$"] = "fn", fn = {"lib", "test"}}
            local actual = L.fn "lib@test"
            assert.are.same(expected, actual)
        end)
        it("parses local call with single positional argument", function()
            local expected = {["$"] = "fn", fn = {"local", "test"}, arg = {["0"] = "one"}}
            local actual = L.fn "test" {"one"}
            assert.are.same(expected, actual)
        end)
        it("parses local call with multiple positional arguments", function()
            local expected = {
                ["$"] = "fn",
                fn = {"local", "test"},
                arg = {["0"] = "one", ["1"] = "zwei", ["2"] = "san"}
            }
            local actual = L.fn "test" {"one", "zwei", "san"}
            assert.are.same(expected, actual)
        end)
        it("parses local call with single keyword argument", function()
            local expected = {["$"] = "fn", fn = {"local", "test"}, arg = {["first"] = "A"}}
            local actual = L.fn "test" {first = "A"}
            assert.are.same(expected, actual)
        end)
        it("parses local call with multiple keyword arguments", function()
            local expected = {
                ["$"] = "fn",
                fn = {"local", "test"},
                arg = {["first"] = "A", ["second"] = "B", ["last"] = "Z"}
            }
            local actual = L.fn "test" {first = "A", second = "B", last = "Z"}
            assert.are.same(expected, actual)
        end)
        it("parses local call with mixed arguments", function()
            local expected = {
                ["$"] = "fn",
                fn = {"local", "test"},
                arg = {["0"] = "one", ["1"] = "two", ["penultimate"] = "nine", ["ultimate"] = "ten"}
            }
            local actual = L.fn "test" {"one", "two", penultimate = "nine", ultimate = "ten"}
            assert.are.same(expected, actual)
        end)
        it("catches local call with invalid second call", function()
            assert.has_error(function() L.fn "test" "what is this?" end, "Invalid argument type")
        end)

        it("parses library call with single positional argument", function()
            local expected = {["$"] = "fn", fn = {"lib", "test"}, arg = {["0"] = "one"}}
            local actual = L.fn "lib@test" {"one"}
            assert.are.same(expected, actual)
        end)
        it("parses library call with multiple positional arguments", function()
            local expected = {
                ["$"] = "fn",
                fn = {"lib", "test"},
                arg = {["0"] = "one", ["1"] = "zwei", ["2"] = "san"}
            }
            local actual = L.fn "lib@test" {"one", "zwei", "san"}
            assert.are.same(expected, actual)
        end)
        it("parses library call with single keyword argument", function()
            local expected = {["$"] = "fn", fn = {"lib", "test"}, arg = {["first"] = "A"}}
            local actual = L.fn "lib@test" {first = "A"}
            assert.are.same(expected, actual)
        end)
        it("parses library call with multiple keyword arguments", function()
            local expected = {
                ["$"] = "fn",
                fn = {"lib", "test"},
                arg = {["first"] = "A", ["second"] = "B", ["last"] = "Z"}
            }
            local actual = L.fn "lib@test" {first = "A", second = "B", last = "Z"}
            assert.are.same(expected, actual)
        end)
        it("parses library call with mixed arguments", function()
            local expected = {
                ["$"] = "fn",
                fn = {"lib", "test"},
                arg = {["0"] = "one", ["1"] = "two", ["penultimate"] = "nine", ["ultimate"] = "ten"}
            }
            local actual = L.fn "lib@test" {"one", "two", penultimate = "nine", ultimate = "ten"}
            assert.are.same(expected, actual)
        end)
        it("catches library call with invalid second call", function()
            assert.has_error(function() L.fn "lib@test" "what is this?" end, "Invalid argument type")
        end)
    end)

    describe("branch control", function()
        it("handles simple if-then BRANCH", function()
            local expected = {
                ["$"] = "kwd",
                fn = "branch",
                val = {
                    {["$"] = "br", fn = "if", val = {{["$"] = "fn", fn = {"local", "foo"}}}},
                    {["$"] = "br", fn = "then", val = {{["$"] = "fn", fn = {"local", "bar"}}}}
                }
            }
            local actual = L.BRANCH {L.IF "foo", L.THEN {L.fn "bar"}}
            assert.are.same(expected, actual)
        end)
        it("handles simple shortif", function()
            local expected = {
                ["$"] = "kwd",
                fn = "shortif",
                ["if"] = {{["$"] = "fn", fn = {"local", "foo"}}},
                ["then"] = {{["$"] = "fn", fn = {"local", "bar"}}}
            }
            local actual = L.IF "foo" {L.fn "bar"}
            assert.are.same(expected, actual)
        end)
        it("rejects empty branch",
           function()
            assert.has_error(function() L.BRANCH {} end, "Unexpected end of branch")
        end)
        it("rejects if without then", function()
            assert.has_error(function() L.BRANCH {L.IF "foo"} end, "Unexpected end of branch")
        end)
        it("rejects elif without then", function()
            assert.has_error(function() L.BRANCH {L.IF "foo", L.THEN {}, L.ELIF "bar"} end,
                             "Unexpected end of branch")
        end)
        it("rejects elif without if first", function()
            assert.has_error(function() L.BRANCH {L.ELIF "bar", L.THEN {}} end,
                             "Unexpected branch elif, expected if")
        end)
        it("rejects then following another then", function()
            assert.has_error(function() L.BRANCH {L.IF "foo", L.THEN {}, L.THEN {}} end,
                             "Unexpected branch then, expected 0 or elif or else")
        end)
        it("rejects else following another else", function()
            assert.has_error(function()
                L.BRANCH {L.IF "foo", L.THEN {}, L.ELSE "baz", L.ELSE "baz"}
            end, "Unexpected branch else, expected 0")
        end)
        it("rejects else between if-then and elif-then", function()
            assert.has_error(function()
                L.BRANCH {L.IF "foo", L.THEN {}, L.ELSE "baz", L.ELIF "bar", L.THEN {}}
            end, "Unexpected branch elif, expected 0")
        end)
    end)

    describe("arg keyword", function()
        it("parses passed positional argument", function()
            local expected = {["$"] = "arg", val = 1}
            local actual = L.arg "1"
            assert.are.same(expected, actual)
            expected = {["$"] = "arg", val = 8}
            actual = L.arg "8"
            assert.are.same(expected, actual)
        end)
        it("parses passed keyword argument", function()
            local expected = {["$"] = "kwarg", val = "word"}
            local actual = L.arg "word"
            assert.are.same(expected, actual)
        end)
        it("parses passed numerical positional argument", function()
            local expected = {["$"] = "arg", val = 4}
            local actual = L.arg(4) -- This technically does not need to be supported
            assert.are.same(expected, actual)
        end)
        it("catches bad argument", function() assert.has_error(function() L.arg {1} end) end)
    end)
end)
