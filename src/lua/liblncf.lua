-- This table will contain the global keywords that may be used within .lncf.lua files
local export = {}

-- This table will contain the flows the user defines through flow()
local flows = {}

export.flows = flows

-- Tries to trim spaces from the start and end of strings
---@param s string
---@return string
local function trim(s) return (s:gsub("^%s*(.-)%s*$", "%1")) end

-- Constructs a set from an array, where a set is simply a table where every value is true
---@param list string[]
---@return table<string, boolean>
local function set(list)
    local _set = {}
    for _, l in ipairs(list) do _set[l] = true end
    return _set
end

-- Set of valid branch members.
local validbranches = set({"if", "elif", "then", "else"})

local urand = assert (io.open ('/dev/urandom', 'rb'))
local function RNG(bytes, mask)
    local b = bytes or 4
    local m = mask or 256
    local n, s = 0, urand:read (b)

    for i = 1, s:len () do
        n = m * n + s:byte (i)
    end

    return n
end
export.key = RNG()

--[[
    A common "argparser" function alias.
    These functions are passed the `delegate` value, which is the object given
    by the user when invoking the keyword, i.e. the string "help" when using
    the keyword EXECUTE by using:
    EXECUTE "help"

    Due to the nature of lua shorthand function calls, only tables and strings
    should really end up here, but in case the uses manually invokes the keyword
    with (), all types should be handled (but anything other than tables or strings
    should, in most case, be an error).

    The value of `self` can be used to modify the defaults the keyword_factory
    function assigns, that is, the values of $ and fn.

    argparser functions are expected to return a modified value of `self`, with
    modifications according to the keyword's specification.
]]
---@alias Argparser fun(delegate: table|string, self: table):table

--[[
    A common keyword function definition. This is simply a function that takes
    a single table or string argument to accomodate for lua shorthand function calls,
    applies some operation to it according to the argument given, and returns a table
    that will be inserted into the final .lncfs.json format.
    Some keyword functions will return a function that will make them REQUIRED to be
    "called twice", while some will define a __call metatable function, making them
    able to be called one OR many times.
]]
---@alias Keyword fun(argument: table|string):table|Keyword

--[[
    Parses both positional and keyword-based arguments into a single table, where positional
    arguments are given an index number.

    {first, second, keyword=value, another=fourth} becomes
    {0=first, 1=second, keyword=value, another=fourth}
    where number-based keys are actually strings ("0", "1", etc)

    When passed an empty table {}, returns nil
]]
---@param tab table<number|string, any>
---@return table<string, any>|nil
local function argparse(tab)
    if (type(tab) ~= "table") then error("Invalid argument type", 2) end
    local ret = {}
    local n = 0
    for i, val in pairs(tab) do
        if type(i) == "number" then
            -- Start positional arguments at 0
            ret[tostring(i - 1)] = val
        else
            ret[i] = val
        end
        -- Count members manually as # is inconsistent
        n = n + 1
    end
    if n == 0 then return nil end
    return ret
end

--[[
    A shorthand definition function for keywords. This assumes some things by
    default but can be configured extensibly
]]
---@param keyword string: The "name" of the keyword (set as .fn). Mandatory
---@param kwd_type string: The "$ type" of the special object, defaults to "kwd"
---@param argparse_callback Argparser
local function _keyword_factory(keyword, kwd_type, argparse_callback)
    return function(delegate)
        local ret = {["$"] = kwd_type, fn = keyword}
        return argparse_callback(delegate, ret)
    end
end

-- sets defaults for _keyword_factory and also lets you skip some arguments
---@see _keyword_factory
local function keyword_factory(argobj)
    return _keyword_factory(argobj[1], -- mandatory, maybe check it exists?
    argobj.kwd or "kwd", argobj.callback or argparse)
end

--[[
    A function call of an external library function or a local flow. This keyword is
    either called once of twice, where the first call is a string, which is the name
    of the function called (in the form `namespace@function` to refer to a function in
    the namespace `namespace` or just `function` to refer to a function in the local
    flow namespace).
    The second call passes arguments to the functions, passed into argparse.
]]
---@param funcheader string: `namespace@function` or `function`
---@return table: Result $ fn call with a __call metatable entry
function export.fn(funcheader)
    local p, namespace, funcname, ret_table
    if (type(funcheader) ~= "string") then
        error("Invalid fn name " .. tostring(funcheader))
    end
    -- split the name at an @ sign
    p = funcheader:gmatch("([^@]+)")
    namespace = p()
    funcname = p()
    -- if there was no part "after" the @, it means there was no @ to begin with
    if not funcname then funcname, namespace = namespace, "local" end
    ret_table = {["$"] = funcname:find("!$") and "fn!" or "fn", fn = {namespace, funcname}}
    return secsmt(export.key)(ret_table, {
        -- A metatable makes this second call optional, and it won't show up in the serialization.
        __call = function(t, delegate)
            t.arg = argparse(delegate)
            return t
        end
    })
end

--[[
    Defines a new flow by the user. This keyword needs to be called twice, where the
    first call takes a string as the flow name and the second call takes the function
    or functions carried out by the flow. Whether the flow is mutating or not is
    automatically determined by the name of the flow (whether or not it ends with !).
]]
---@param flowname string
---@return Keyword
function export.flow(flowname)
    -- Validate flow names here?
    if (type(flowname) ~= "string") then error("Invalid flow name " .. tostring(flowname)) end
    local flowkey = flowname:find("!$") and "flow!" or "flow"
    return function(flow_funcs) flows[flowname] = {[flowkey] = flow_funcs} end
end

--[[
    Refers to an argument passed into the flow, if it is called by another flow. These
    are either numerical, refering to positional arguments, starting at one, whereas
    keyword arguments simply use the name of the argument.
    Because lua only accepts strings or tables as a function call shorthand, those
    numerical arguments must also be represented as string, for example:
    arg "1", arg "2", arg "4"
    keyword arguments:
    arg "foo", arg "bar", arg "word"
    The argument word is passed to all mutating flows.
    Note that the function still handles numerical functions anyway, in case it is
    called with explicit notation, such as arg(1), arg(2), etc..
]]
---@param argnum string|number
---@return table
function export.arg(argnum)
    local argtype
    if (type(argnum) == "string") then
        -- Attempts to convert argnum to a number
        local argnum_tonumber = tonumber(argnum)
        -- argnum = argnum_tonumber ?? argnum
        -- tonumber returns nil in case the given string isn't a number, which means its
        -- a keyword argument.
        argnum = (argnum_tonumber == nil) and argnum or argnum_tonumber
    elseif (type(argnum) ~= "number") then
        error("Invalid argument number " .. tostring(argnum))
    end
    argtype = (type(argnum) == "number") and "arg" or "kwarg"
    return {["$"] = argtype, val = argnum}
end

--[[
    A temporary argparser that keeps data but doesn't verify, modify
    or categorize in any way. ONLY MEANT TO BE USED TEMPORARILY
]]
---@type Argparser
local function temp_argparse_wrap(delegate_in, passed)
    if (type(delegate_in) ~= "table") then
        passed.val = {export.fn(delegate_in)}
    else
        passed.val = delegate_in
    end
    return passed
end

--[[
    A temporary argparser that must be called twice and discard both
    call informations. ONLY MEANT TO BE USED TEMPORARILY
]]
---@type Argparser
-- local function temp_argparse_discard(_, passed) return function() return passed end end

--[[
    An argparser that can parse two kinds of string shorthands for "if" expressions,
    in addition to simple arrays.
    The string shorthand may begin with @, in which case it is an operator shorthand,
    which means the content after the @ is meant to be parsed by the operator parser
    later on in the generate step. This is useful for complex conditional expressions
    such as:
    @ foo == 1 || bar == "baz"
    If the string doesn't begin with an @, it is simply read as a function name, and
    passed onto fn(), except without arguments. This makes it easy to refer to
    functions that return a boolean and take no parameters.
    In other cases, this takes an array (of functions to execute in the body) which
    is not processed in any way.
]]
---@type Argparser
local function operator_shorthand_wrap(delegate_in, passed)
    if (type(delegate_in) == "string") then
        if (#delegate_in == 0) then
            error("Operator shorthand may not be blank")
        elseif (delegate_in:sub(1, 1) == "@") then
            passed.val = {{["$"] = "op", val = trim(delegate_in:sub(2))}}
        else
            passed.val = {export.fn(delegate_in)}
        end
    elseif (type(delegate_in) == "table") then
        passed.val = delegate_in
    else
        error("Invalid operator shorthand type " .. type(delegate_in))
    end
    return passed
end

--[[
    This is a special case parser only for the keyword IF because of its dual
    nature. On its own, this parser simply uses operator_shorthand_wrap, but
    modified its result so that it's $ type is "shortif" and the resulting
    body given by the operator_shorthand_wrap is put under the key "if".
    Also, it adds a positional value that makes it impossible to serialize.
    This is because on its own, shortif simply isn't a valid keyword.
    If must either be called a second time, in order to be handled as a
    shorthand notation, or be called inside BRANCH, in order to be handled
    as a branch member.
]]
---@type Argparser
local function shortif_wrap(delegate_in, passed)
    passed = operator_shorthand_wrap(delegate_in, passed)
    passed["if"], passed.val = passed.val, nil
    passed[0] = "shortif" -- Invalidate for serialization
    -- Revalidated by either the __call below or by `branch_wrap`
    return secsmt(export.key)(passed, {
        __call = function(t, delegate)
            t["$"] = "kwd"
            t["then"] = operator_shorthand_wrap(delegate, {}).val
            t[0] = nil -- Validate for serialization again.
            return t
        end
    })
end

--[[
    Special wrapper only for the keyword BRANCH. It validates its branch members to make
    sure they are valid, and also modifies any IF keywords to make it into a branch rather
    than a shorthand keyword (see `shortif_wrap`).
    TODO: should probably make sure THEN follows IF, etc
]]
---@type Argparser
local function branch_wrap(delegate_in, passed)
    if (type(delegate_in) ~= "table") then
        error("Invalid branch structure type " .. type(delegate_in))
    end
    passed.val = {}
    local expected = set {"if"}

    for j, i in pairs(delegate_in) do
        local expectedstr = {}

        for k, _ in pairs(expected) do table.insert(expectedstr, k) end
        table.sort(expectedstr, function(a, b) return tostring(a) < tostring(b) end)
        expectedstr = table.concat(expectedstr, " or ")
        expectedstr = expectedstr == "" and "nothing" or expectedstr
        if (type(j) ~= "number") then error("Invalid branch list (type " .. type(j) .. ")") end
        if (i["$"] ~= "br") then
            if (i["$"] == "shortif" and i["fn"] == "shortif") then
                i.val, i["if"], i["$"], i["fn"] = i["if"], nil, "br", "if"
                i[0] = nil -- Revalidate for serialization
            else
                error("Invalid branch member $ type " .. i["$"])
            end
        end
        if (not validbranches[i["fn"]]) then error("Invalid kind of branch " .. i["fn"]) end
        if (not expected[i["fn"]]) then
            error("Unexpected branch " .. i["fn"] .. ", expected " .. expectedstr)
        end
        if (i["fn"] == "if") then
            expected = set {"then"}
        elseif (i["fn"] == "elif") then
            expected = set {"then"}
        elseif (i["fn"] == "then") then
            expected = set {"elif", "else", 0}
        elseif (i["fn"] == "else") then
            expected = set {0}
        end
        table.insert(passed.val, i)
    end
    if (not expected[0]) then error("Unexpected end of branch") end
    return passed
end

--[[
    Special wrapper only for the keyword MACRO. As everything that MACRO does is
    left to be dealt with by the compiler, the parser for it is fairly simple
    and copied almost entirely from `fn()`.
    Similar to fn, it passes delegate tables through `argparse()`, to allow through
    possible "sparse tables" that wouldn't otherwise be serialized. All other types
    are also allowed and passed through directly.
]]
---@param macroheader string
function export.MACRO(macroheader)
    local p, namespace, macroname, ret_table
    if (type(macroheader) ~= "string") then
        error("Invalid macro name " .. tostring(macroheader))
    end
    -- split the name at an @ sign
    p = macroheader:gmatch("([^@]+)")
    namespace = p()
    macroname = p()
    -- unlike fn, "local" macros aren't currently valid
    if not macroname then error("Macros must refer to an external namespace") end
    ret_table = {["$"] = macroname:find("!$") and "macro!" or "macro", fn = {namespace, macroname}}
    return secsmt(export.key)(ret_table, {
        -- A metatable makes this second call optional, and it won't show up in the serialization.
        __call = function(t, delegate)
            if (type(delegate) == "table") then
                t.arg = argparse(delegate)
            else
                t.arg = delegate
            end
            return t
        end
    })
end

-- An ""argparser"" that recursively disards all passed in values. ONLY MEANT TO BE USED TEMPORARILY
function undefined() return secsmt(export.key)({}, {__call = undefined}) end

export.class_vector = undefined
export.classify = undefined
export.func_vector = undefined
export.func_space = undefined
export.match = undefined
export.case = undefined

export.BRANCH = keyword_factory {"branch", callback = branch_wrap}
export.IF = keyword_factory {"shortif", kwd = "shortif", callback = shortif_wrap}
export.ELIF = keyword_factory {"elif", kwd = "br", callback = operator_shorthand_wrap}
export.THEN = keyword_factory {"then", kwd = "br", callback = temp_argparse_wrap}
export.ELSE = keyword_factory {"else", kwd = "br", callback = temp_argparse_wrap}

export.SUFFIX = undefined
export.INCLUDE = undefined
export.EXCLUDE = undefined
export.PERMUTE = undefined

return export
