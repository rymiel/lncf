function test! ( ; word::String )::String
  if (Lib.contains(
  )) begin
    word = Lib.replace!(
      "abc",
      "ABC";
      word=word
    )
    end
  else begin
    word = Local.something_else!(
      "positional";
      keyword = "argument"
      word=word
    )
    end
  end
end