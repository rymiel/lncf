import { expect } from "chai";
import fs from "fs";
import path from "path";
import { compile } from "../compile";
import exec from "@jsdevtools/ez-spawn";
import { validate } from "../validation";

const emitJson = (testCaseName: string) =>
  exec.sync(`../../scripts/test/emit-json.sh ${testCaseName}`).stdout;

const fileAsString = (filename: string) =>
  fs.readFileSync(path.resolve(__dirname, filename + ".out.txt.jl"), {
    encoding: "utf-8",
  });

describe("compile", () => {
  it("should handle codegentest case", () => {
    let validation = validate(emitJson("codegentest"));
    expect(validation.success).to.be.true;
    if (!validation.success) return;
    expect(validation.result.code).to.equal("OK");
    if (validation.result.code !== "OK") return;
    const result = compile(validation.result.output);
    expect(result).to.equal(fileAsString("example"));
  });
});
