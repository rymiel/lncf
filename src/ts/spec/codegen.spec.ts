import { CodeGen } from "../codegen/index";
import { expect } from "chai";
import fs from "fs";
import path from "path";

const fileAsString = (filename: string) =>
  fs.readFileSync(path.resolve(__dirname, filename + ".out.txt.jl"), {
    encoding: "utf-8",
  });

describe("codegen", () => {
  it("should handle example case", () => {
    const gen = new CodeGen();
    gen.flow(
      "test!",
      0,
      [],
      [
        gen.if(
          gen.call("Lib.contains", [], {}, false),
          [
            gen.call(
              "Lib.replace!",
              [gen.literal("abc"), gen.literal("ABC")],
              {},
              true
            ),
          ],
          [],
          [
            gen.call(
              "Local.something_else!",
              [gen.literal("positional")],
              { keyword: gen.literal("argument") },
              true
            ),
          ]
        ),
      ],
      true
    );
    expect(gen.toString()).to.equal(fileAsString("example"));
  });
});
