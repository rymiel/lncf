import exec from "@jsdevtools/ez-spawn";
import { expect } from "chai";
import { validate } from "../validation";
import fs from 'fs';

const examples = new Map<string, string>();
fs.readdirSync(`../../examples/`).forEach((file) => {
  file = file.replace(/^(\w+)\.lncf\.lua$/, '$1');
  examples.set(file, exec.sync(`../../scripts/test/emit-json.sh ${file}`).stdout);
});

describe("validate", () => {
  const multiplex = (configs: string[], expectPass: Boolean = true) => {
    for (const c of configs) {
      it(c, async () => {
        let validation = validate(examples.get(c));
        if (!expectPass) {
          expect(validation.success).to.be.false;
        } else {
          expect(validation.success).to.be.true;
        }
      })
    }
  }
  describe("allowing valid config files", () => {
    multiplex(["solerian", "example"]);
  });
  describe("disallowing invalid config files", () => {
    multiplex(["invalid"], false);
  });
});
