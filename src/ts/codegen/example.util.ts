import { CodeGen } from "./index";

export const gen = new CodeGen();
gen.flow(
  "test!",
  0,
  [],
  [
    gen.if(
      gen.call("Lib.contains", [gen.literal("abc")], {}, false),
      [
        gen.call(
          "Lib.replace!",
          [gen.literal("abc"), gen.literal("ABC")],
          {},
          true
        ),
      ],
      [],
      [
        gen.call(
          "Local.something_else!",
          [gen.literal("positional")],
          { keyword: gen.literal("argument") },
          true
        ),
      ]
    ),
  ],
  true
);
console.log(gen.toString());
