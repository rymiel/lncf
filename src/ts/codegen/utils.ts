export function delay<T>(node: T): Delayed<T> {
  return () => node;
}

export interface ExtGenOpts {
  tabCharacter?: string;
};

export const defaultExtGenOpts: ExtGenOpts = {
  tabCharacter: "  ",
};

export interface GenOpts extends ExtGenOpts {
  _n: "\n";
  _t: number;
};

export type Name = string;

export type Delayed<T> = () => T;

export class Builder {
  private content: string[] = [];
  constructor(seedContent: string = "", protected _t: number = 0) {
    this.content = [seedContent];
  }

  add(...strings: string[]): void {
    this.content.push(...strings);
  }

  indent(): number {
    return ++this._t;
  }

  dedent(): number {
    return --this._t;
  }

  tab(tab: string = "  "): string {
    return tab.repeat(this._t);
  }

  toString(): string {
    return this.content.join("");
  }
}
