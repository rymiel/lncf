import { inspect } from "util";
import {
  Builder,
  Delayed,
  GenOpts,
  Name,
  ExtGenOpts,
  defaultExtGenOpts,
  delay,
} from "./utils";

class CodeBuilder extends Builder {
  constructor(private _opts: GenOpts) {
    super("", _opts._t);
  }

  indent(): number {
    this._opts._t++;
    return ++this._t;
  }

  dedent(): number {
    this._opts._t--;
    return --this._t;
  }

  line(...strings: string[]): void {
    this.add(this.n, this.t, ...strings);
  }

  get t(): string {
    return this._opts.tabCharacter.repeat(this._t);
  }

  get n(): string {
    return this._opts._n;
  }
}

abstract class Node {
  abstract render(opts: GenOpts): string;
  abstract readonly name: string;

  get delay(): Delayed<this> {
    return delay(this);
  }
}

abstract class BuiltNode extends Node {
  abstract build(s: CodeBuilder, opts: GenOpts): void;

  render(opts: GenOpts): string {
    const s = new CodeBuilder(opts);
    this.build(s, opts);
    return s.toString();
  }
}

class Argument extends BuiltNode {
  name = "argument";

  constructor(
    readonly index: number | string,
    readonly value: Delayed<ChildNode>
  ) {
    super();
  }

  build(s: CodeBuilder, opts: GenOpts) {
    if (typeof this.index === "number") {
      s.add(this.value().render(opts));
    } else {
      s.add(this.index, " = ", this.value().render(opts));
    }
  }
}

abstract class TreeNode<T extends Node> extends Node {
  readonly call_nodes: Delayed<T>[] = [];

  constructor(readonly nodes: Delayed<T>[] = []) {
    super();
  }

  render(opts: GenOpts): string {
    return this.nodes.reduce((code, n) => code + n().render(opts), "");
  }
}

abstract class ParentNode extends TreeNode<ChildNode> {}

class Root extends TreeNode<Flow> {
  name = "root";
}

class If extends ParentNode {
  name = "if";

  constructor(
    private condition: Delayed<ChildNode>,
    private otherClauses?: Delayed<Elif>[],
    private elseClause?: Delayed<Else>,
    nodes?: Block
  ) {
    super(nodes);
  }

  render(opts: GenOpts): string {
    const s = new CodeBuilder(opts);
    s.add("if (", this.condition().render(opts), ") begin");
    s.indent();
    this.nodes.forEach((n) => s.line(n().render(opts)));
    s.line("end");
    s.dedent();
    // TODO: elif and else clauses
    if (this.elseClause) {
      s.line("else begin");
      s.indent();
      s.line(this.elseClause().render(opts));
      s.line("end");
      s.dedent();
    }
    s.line("end");
    return s.toString();
  }
}

class Elif extends ParentNode {
  name = "elif";

  constructor(readonly condition: Delayed<ChildNode>, nodes?: Block) {
    super(nodes);
  }
}

class Else extends ParentNode {
  name = "else";
}

class Call extends TreeNode<Argument> {
  name = "call";

  constructor(
    readonly func: Name,
    readonly mutating: boolean,
    nodes?: Delayed<Argument>[]
  ) {
    super(nodes);
  }

  render(opts: GenOpts): string {
    const s = new CodeBuilder(opts);
    s.add(this.mutating ? `word = ` : ``, `${this.func}(`);
    if (this.nodes.length > 0) {
      s.indent();
      const posArgs: string[] = [];
      const kwdArgs: string[] = [];
      this.nodes.forEach((element) => {
        const i = element();
        const pushTo = typeof i.index === "number" ? posArgs : kwdArgs;
        pushTo.push(s.n + s.t + i.render(opts));
      });
      s.add(posArgs.join(","));
      s.add(";");
      s.add(kwdArgs.join(","));
      if (this.mutating) {
        s.line("word=word");
      }
      s.dedent();
    }
    s.line(")");
    return s.toString();
  }
}

class Flow extends ParentNode {
  name = "flow";

  constructor(
    readonly func: Name,
    private positionals: number,
    private param: string[] = [],
    nodes?: Block
  ) {
    super(nodes);
  }

  render(opts: GenOpts): string {
    const s = new CodeBuilder(opts);
    s.add(`function ${this.func} ( `);
    s.add(
      Array(this.positionals)
        .fill(null)
        .map((_, i) => `arg${i}`)
        .join(", ")
    );
    s.add("; ");
    s.add(
      this.param.map((i) => `${i}${i === "word" ? "::String" : ""}`).join(", ")
    );
    // TODO: argument typing that isn't an explicit check for "word"
    s.add(" )::String");
    s.indent();
    this.nodes.forEach((n) => s.line(n().render(opts)));
    s.dedent();
    s.line("end");
    return s.toString();
  }
}

class Literal<T> extends Node {
  name = "literal";

  constructor(readonly val: T) {
    super();
  }

  render(opts: GenOpts): string {
    return typeof this.val === "string" ? `"${this.val}"` : `${this.val}`;
  }
}

type PrimitiveLiterals = string | number | boolean;
type Leaf = Literal<PrimitiveLiterals>;
type BlockNode = If | Call | Flow;
type ChildNode = BlockNode | Leaf;
export type BlockComponent = Delayed<ChildNode>;
export type Block = BlockComponent[];

export class CodeGen {
  private _root: Root;
  private readonly opts: GenOpts;

  constructor(opts: ExtGenOpts = {}) {
    this.opts = { ...defaultExtGenOpts, ...opts, _n: "\n", _t: 0 };
    this._root = new Root();
  }

  toString(): string {
    return this._root.render(this.opts);
  }

  flow(
    name: Name,
    positionals: number,
    param: string[],
    body: Block,
    mutating: boolean
  ) {
    if (mutating) param.push("word");
    const flow = new Flow(name, positionals, param, body);
    this._root.nodes.push(() => flow);
    return flow;
  }

  if(
    condition: Delayed<ChildNode>,
    thenBody: Block,
    elifBody?: [Delayed<ChildNode>, Block][],
    elseBody?: Block
  ) {
    if (!thenBody && elseBody) {
      throw new Error('codegen: "else" body without "then" body');
    }
    return new If(
      condition,
      elifBody?.map<Delayed<Elif>>(
        ([iIf, iThen]) => new Elif(iIf, iThen).delay
      ),
      delay(new Else(elseBody)) ?? undefined,
      thenBody
    ).delay;
  }

  call(
    name: Name,
    args: Block,
    kwargs: Record<Name, Delayed<ChildNode>>,
    mutating: boolean
  ) {
    // Verify namespace stuff here...?
    const body = [
      ...args.map((j, i) => () => new Argument(i, j)),
      ...Object.entries(kwargs).map(([i, j]) => () => new Argument(i, j)),
    ];
    return new Call(name, mutating, body).delay;
  }

  literal(value: PrimitiveLiterals) {
    return new Literal(value).delay;
  }

  get root() {
    return inspect(this._root);
  }
}
