const tsj = require("ts-json-schema-generator");
const fs = require("fs");
const path = require("path");

const config = {
    path: path.resolve(__dirname, "interface.ts"),
    tsconfig: path.resolve(__dirname, "../tsconfig.json"),
    type: "Result",
};

const output_path = path.resolve(__dirname, "schema.json");

const schema = tsj.createGenerator(config).createSchema(config.type);
const schemaString = JSON.stringify(schema, null, 2);
fs.writeFile(output_path, schemaString, (err) => {
    if (err) throw err;
});