import Ajv, { ErrorObject } from "ajv";
// TODO: This schema import should also come from a parameter somewhere to ensure pipeline-safety,
// that is, the pipeline should make no files (that aren't temporary and random) in order to not
// interfere with other pipelines, so they could be run concurrently from the same source.
import schema from "./schema.json";
import { Result } from "./interface.util";

interface Validation<S extends boolean, T> {
  success: S;
  result: T;
}

export type ValidationResult =
  | Validation<true, Result>
  | Validation<false, ErrorObject<string, Record<string, any>>[]>;

var ajv = new Ajv(); // options can be passed, e.g. {allErrors: true}
var _validate = ajv.compile(schema);

export function validate(input: string): ValidationResult {
  var data = JSON.parse(input);
  var valid = _validate(data);
  if (!valid && _validate.errors) {
    return { success: false, result: _validate.errors };
  } else {
    const result: Result = data;
    return { success: true, result: result };
  }
}
