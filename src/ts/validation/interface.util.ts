interface ResultObj<T extends string> {
  code: T;
  status: string;
}

interface PartialResult<T extends string> extends ResultObj<T> {
  output: LNCFStructure;
}

export type Result =
  | PartialResult<"OK">
  | PartialResult<"ERROR">
  | ResultObj<"ABORT">;

export interface LNCFStructure {
  flows: Record<
    FuncName,
    Record<"flow", Flow<Body>> | Record<"flow!", Flow<MutBody>>
  >;
}

export type FuncName = string;
export type Namespace = string;
export type ParamKey = string;

export type Flow<M extends Body | MutBody> = M;

export type BodyComponent = Func | Macro | Kwd<Body>
export type Body = [BodyComponent];
export type MutBodyComponent = MutFunc | MutMacro | Kwd<MutBody>
export type MutBody = MutBodyComponent[];

interface TypeObj<T extends string> {
  $: T;
}

interface FuncLike<T extends string> extends TypeObj<T> {
  fn: [Namespace, FuncName];
}

interface FuncObj<T extends string> extends FuncLike<T> {
  arg?: Record<ParamKey, Literal>;
}

type Func = FuncObj<"fn">;
type MutFunc = FuncObj<"fn!">;

interface Macro extends FuncLike<"macro"> {
  arg?: Record<ParamKey, Literal> | Literal;
}

interface MutMacro extends FuncLike<"macro!"> {
  arg?: Record<ParamKey, FreeLiteral> | FreeLiteral;
}

export type Kwd<M extends Body | MutBody> =
  | ShortIf<M>
  | Branch<M>
  | Macro;

interface KwdObj<T extends string> extends TypeObj<"kwd"> {
  fn: T;
}

interface BrObj<T extends string> extends TypeObj<"br"> {
  fn: T;
}

export interface ShortIf<M extends Body | MutBody> extends KwdObj<"shortif"> {
  if: Body | [Operator];
  then: M
}

export interface Branch<M extends Body | MutBody> extends KwdObj<"branch"> {
  val: BranchTypes<M>[];
}

export interface IfBranch extends BrObj<"if"> {
  val: Body | [Operator];
}

export interface ThenBranch<M extends Body | MutBody> extends BrObj<"then"> {
  val: M;
}

export interface ElifBranch extends BrObj<"elif"> {
  val: Body | [Operator];
}

export interface ElseBranch<M extends Body | MutBody> extends BrObj<"else"> {
  val: M;
}

export type BranchTypes<M extends Body | MutBody> = IfBranch | ThenBranch<M> | ElifBranch | ElseBranch<M>;

export interface Operator extends TypeObj<"op"> {
  val: string;
}

export interface Arg extends TypeObj<"arg"> {
  val: number;
}

export interface Kwarg extends TypeObj<"kwarg"> {
  val: string;
}

type DisallowedAttributes = { [k in '$']?: void };

export type Literal =
  | Func
  | Macro
  | Kwd<Body>
  | Arg
  | Kwarg
  | string
  | number
  | boolean
  | Literal[]
  | { [k in string]: Literal} & DisallowedAttributes;

export type FreeLiteral =
  | Literal
  | MutFunc
  | MutMacro
  | Kwd<MutBody>
  | FreeLiteral[]
  | { [k in string]: FreeLiteral} & DisallowedAttributes;
