import { Block, BlockComponent, CodeGen } from "../codegen";
import {
  Body,
  BodyComponent,
  ElifBranch,
  ElseBranch,
  IfBranch,
  Literal,
  LNCFStructure,
  MutBody,
  MutBodyComponent,
  ThenBranch,
} from "../validation/interface.util";

const capitalize = (s: string) => s.charAt(0).toUpperCase() + s.slice(1);

export function compile(structure: LNCFStructure) {
  const gen = new CodeGen();

  function separateArguments(
    args: Record<string, Literal>
  ): [BlockComponent[], Record<string, BlockComponent>] {
    let n = 0;
    let keys = Object.keys(args);
    let posargs = [];
    let kwargs: Record<string, BlockComponent> = {};
    while (true) {
      const name = n.toString()
      if (keys.includes(name)) {
        posargs.push(compileLiteral(args[name]));
        keys = keys.filter(e => e !== name)
        n++;
      } else break;
    }
    for (const k of keys) {
      kwargs[k] = compileLiteral(args[k]);
    }
    return [posargs, kwargs];
  }
  
  function compileLiteral(c: Literal): BlockComponent {
    if (typeof c == "string") {
      return gen.literal(c);
    }
    return;
  }
  
  function compileComponent(c: BodyComponent | MutBodyComponent): BlockComponent {
    if (c.$ === "fn" || c.$ === "fn!") {
      const mutating = c.$ === "fn!";
      // This merely "emulates" a julia function name
      // TODO: Actual function lookup
      const name = capitalize(c.fn.join(".").replace(/ /g, "_"));
      const [args, kwargs] = separateArguments(c.arg ?? {});
      return gen.call(name, args, kwargs, mutating);
    } else if (c.$ === "kwd") {
      if (c.fn === "branch") {
        // TODO: Operator handling (currently casted out)
        const ifBranch = (c.val[0] as IfBranch).val as Body;
        const ifBlock = compileBody(ifBranch)[0];
        const thenBranch = (c.val[1] as ThenBranch<Body | MutBody>).val;
        const thenBlock = compileBody(thenBranch);
        let elseBlock: Block;
        // TODO: Operator handling (currently casted out)
        let elifPair: Body;
        let otherBlocks: [BlockComponent, Block][] = [];
        for (let i = 2; i < c.val.length; i++) {
          const b = c.val[i];
          if (b.fn === "elif") {
            elifPair = b.val as Body;
          } else if (b.fn === "then") {
            otherBlocks.push([
              compileBody(elifPair)[0],
              compileBody(b.val)
            ]);
          } else if (b.fn === "else") {
            elseBlock = compileBody(b.val);
          }
        }
        return gen.if(
          ifBlock, thenBlock, otherBlocks, elseBlock
        );
      }
    } else {
      throw new Error(`Component with $type ${c.$} was unrecognized, couldn't compile`);
    }
  }
  
  function compileBody(body: Body | MutBody): Block {
    let block = [];
    for (const i of body) {
      block.push(compileComponent(i));
    }
    return block;
  }

  for (const k in structure.flows) {
    const v = structure.flows[k];
    const mutable = Object.keys(v).includes("flow!");
    const body = mutable // Typescript can probably do something prettier than this
      ? (v as Record<"flow!", MutBody>)["flow!"]
      : (v as Record<"flow", Body>)["flow"];
    let bodyBlock: Block;
    bodyBlock = compileBody(body);
    // TODO: find arguments and add them there
    gen.flow(k, 0, [], bodyBlock, mutable);
  }

  return gen.toString();
}
