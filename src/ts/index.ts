import exec from "@jsdevtools/ez-spawn";
import { compile } from "./compile";
import { validate } from "./validation";
// import { inspect } from 'util';

const example = exec.sync(`../../scripts/test/pretty-emit.sh ${process.argv[2] ?? ''}`).stdout;

const v = validate(example);
if (v.success && v.result.code == "OK") {
    const c = compile(v.result.output);
    console.log(c);
} else {
    console.log("Invalid?");
    console.log(example);
    console.log(v.result)
}
