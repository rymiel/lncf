using Pkg

Pkg.develop(PackageSpec(path=pwd()))
Pkg.build("LncfJulia")
Pkg.add("TestReports")

using TestReports

TestReports.test("LncfJulia"; coverage=true)