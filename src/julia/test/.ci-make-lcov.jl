using Pkg
Pkg.add("Coverage")

using Coverage

coverage = process_folder()
LCOV.writefile("../../.coverage/julia.lcov", coverage)
clean_folder("../")