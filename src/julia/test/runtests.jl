using Test, LncfJulia

@test Lib.map_characters("a", "A"; word="abcbcacba") ≡ "AbcbcAcbA"