module Operator

export or
export eq
or(a, b) = a || b;
eq(a, b) = a == b;

end