function map_characters(x::String, y::String; reverse::Bool=false, limit::Int=0, word::String)::String
    local mapcount = 0
    local charmap = Dict(zip(x, y))
    local outbuffer = IOBuffer(sizehint=length(word))
    local inbuffer = IOBuffer(reverse ? Base.reverse(word) : word)
    while true
        if inbuffer.ptr > inbuffer.size
            break
        end
        local c = read(inbuffer, Char)
        local mapc = get(charmap, c, nothing)
        if mapc isa Nothing
            write(outbuffer, c)
        else
            write(outbuffer, mapc)
            mapcount += 1
            if limit > 0 && mapcount == limit
                write(outbuffer, read(inbuffer, String))
                break
            end
        end
    end
    word = String(take!(outbuffer))
    return reverse ? Base.reverse(word) : word
end
