local stress_regex = "[áéíóúý]"
local vowel_regex = "[eiouyàáéíóúý]"
local syllable_regex = "[aeiouyàáéíóúý]"

flow "destress!" {
    fn "lib@map characters!" {
        "áéíóúý", "àeiouy"
    }
}

flow "stress first!" {
    fn "destress!",
    fn "lib@map characters!" {
        "aàeiouy", "ááéíóúý",
        limit=1
    }
}

flow "stress last!" {
    fn "destress!",
    fn "lib@map characters!" {
        "aàeiouy", "ááéíóúý",
        limit=1, reverse=true
    }
}

flow "lax stress!" {
    fn "lib@map characters!" {
        "àeiouy", "áéíóúý",
        limit=1,
    }
}

flow "stressed" {
    fn "lib@has match" {
        arg "1", stress_regex
    }
}

flow "vowel count" {
    fn "lib@match count" {
        vowel_regex
    }
}

flow "syllable count" {
    fn "lib@match count" {
        syllable_regex
    }
}

flow "normalize!" {
    BRANCH {
        IF "stressed",
        THEN {
            IF "@ syllable count = 1 || vowel count = 1" "destress!"
        },
        ELSE {
            BRANCH {
                IF "@ vowel count = 0",
                THEN "stress first!",
                ELIF "@ vowel count > 1",
                THEN "lax stress!"
            }
        }
    }
}

flow "apply suffix!" {
    IF "@ stressed($1)" "destress!",
    fn "op@append!" {arg "1"},

    MACRO "lib@rune!" {
        {"replace", "@", fn "lib@char" {arg "suffix", 1}},
        {"trigger", "<", fn "stress first!"},
        {"trigger", ">", fn "stress last!"},
    },

    fn "normalize!"
}

class_vector "paradigm" {
    "noun", "verb"
} "manual"

class_vector "noun class" {
    "f1t", "f1d", "f2i", "f2x", "f2", "m1", "m2", "n1", "n2"
} "paradigm:noun"

class_vector "verb class" {
    "0", "I", "II", "III", "IV"
} "paradigm:verb"

classify "noun class" {
    SUFFIX "[àá]t" "f1t",
    SUFFIX "[àá]d" "f1d",
    SUFFIX "[ií]à" "f2i",
    SUFFIX "[àá]x" "f2x",
    SUFFIX "[àá]"  "f2",
    SUFFIX "[eé]n" "m1",
    SUFFIX "m"     "m2",
    SUFFIX "[eé]l" "n1",
    SUFFIX "r"     "n2"
}

classify "verb class" {
    SUFFIX "élus" "I",
    SUFFIX (syllable_regex .. "las") "II",
    SUFFIX "lud" "III",
    INCLUDE {
        EXCLUDE {
            SUFFIX (stress_regex .. "(.*)[nm][úu]")
        },
        SUFFIX "[nm][úu]"
    } "IV",
    SUFFIX "lus" "0"
}

func_vector "number" {
    "sg", "pl"
}

func_vector "case" {
    "nom", "acc", "gen"
}

func_space {
    on = "paradigm:noun",
    dimensions = {"number", "case"},
    order = PERMUTE,
    word = "stripped",
    flow = "apply suffix!",
    match "noun class" {
        case "f1t" {"àt", "en", "is", "àtún", "etin", "iis"},
        case "f1d" {"àd", "ein", "is", "ánd", "etin", "iis"},
        case "f2i" {"à", "e", "r", "áin", "ein", "ir"},
        case "f2x" {"àx", "ox", "ir", "áxi", "oxe", "ixir"},
        case "f2"  {"à", "e", "ir", "áin", "ein", "iir"},
        case "m1"  {"en", "ean", "yr", "enét", "eant", "esyr"},
        case "m2"  {"m", "m", "mer", "mas", "mas", "ǹir"},
        case "n1"  {"el", "aln", "eler", "eek", "alnek", "elsar"},
        case "n2"  {"r", "rin", "ràr", "àr", "rinse", "riser"},
    },
}

func_vector "person" {
    "1", "2", "3"
}

func_vector "tense" {
    "prs", "pst"
}

func_space {
    on = "paradigm:verb",
    dimensions = {"person", "number", "tense"},
    order = {"1st inf", "2nd inf", PERMUTE},
    word = "stripped",
    flow = "apply suffix!",
}
