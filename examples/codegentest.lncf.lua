flow "test!" {
    BRANCH {
        IF {
            fn "lib@contains"
        },
        THEN {
            fn "lib@replace!" {
                "abc", "ABC"
            }
        },
        ELSE {
            fn "something else!" {
                "positional",
                keyword="argument"
            }
        }
    }
}