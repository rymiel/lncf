# LNCF - LaNguage ConFig

[[_TOC_]]

## Description

lncf is a pipeline to convert a simple, readable configuration file describing the forms of words in a language, to a high-performance backend for various kinds of word transformations

The configuration is designed to be simple to write and understand for someone without a technical background, yet stay flexible as a scripting language

## Overview

The input from the end-user is the config is a file with the extension `.lncf.lua`. As the extension suggests, this is actually an actual lua script, sandboxed for safety, with some external functions to describe more complex data structures. The pipeline outputs a Julia file described by the config file. As Julia is compiled just-in-time, this results in fast code for use as a backend.

## Steps

The pipeline passes through 3 types of data representations and 9 overall steps, listed below.

### Data representations

* **`.lncf.lua`** - initial source
* **`.lncfs.json`** - a more machine-indexable representation, with concrete types and structures. Stands for LaNguage ConFig Structure
* **`.lncfc.jl`** - final, performant output code. Stands for LaNguage ConFig Compiled

### Pipeline steps

* **CONFIG** as **.lncf.lua** - Input step of the `.lncf.lua`. Some examples and testing files are located in `examples/`. An __error in this step__ means the user provided a lua file that isn't valid lua syntax.
* **PARSE** - The input lua file is read and interpeted. Contained in `src/lua/sandbox.lua` which is safely wrapped by the `src/lua/lualncf` bash script. An __error in this step__ means the user provided a lua file which throws a runtime error when interpreted. *It could also mean the underlying implementation (liblncf) has a syntax or runtime error*
* **COMPOSE** - The abstract lua interpretation is put together into a concrete table. Contained in `src/lua/liblncf.lua`. An __error in this step__ means the user has used liblncf incorrectly, such as invalid parameters to the functions.
* **EMIT** - Output the table as a json object for communication between languages. Contained in `src/lua/vendor/json.lua`. Note that this step outputs json even when any of the above steps fail, which lets you handle the 4 lua steps as a single block through the `lualncf` shell script. An __error in this step__ means the user has caused created data which cannot be serializable or is otherwise invalid as a simple json structure. *It could also mean the underlying json.lua implementation is flawed*
* **STRUCTURE** as **.lncfs.json** - The json representation handled opaquely between steps.
* **VALIDATION** - Validates the json to find explicit faults that *would* cause problems in the later steps but might be slower or more cryptic. This would be the first step where the user data is scanned over with the full context of the entire output. Contained in `src/ts/validation/`. An __error in this step__ means an error of the kind described above has been located.
* **GENERATE** - The structure definition is iterated an over to produce a julia script through code generation. Contained in `src/ts/compile/`. An __error in this step__ means the structure is invalid, contains illegal calls  or identifiers, or is otherwise unclear about the intended result.
* **CODE** as **.lncfc.jl** - Final resulting code. Contained in `src/julia/lib/` is the library of simple functions to be used in lncf scripts that may be used very repetitively, are cumbersome to implement manually or is otherwise faster via the use of implementations that aren't sandboxed.
* **COMPILE** - Run step of the resulting code using the julia compiler. The first run of julia code compiles it, resulting in blazing fast performance the following times. An __error in this step__ means an unforseeable runtime error such as a type error has occured as not every scenario is validated against, or the generate step has produced code that isn't syntactically valid. Ideally the number of these errors should be near zero to minimize late crashes in the final result. **Better safe than sorry**