# Flows

Transformations on words are realized as **Flows**, defined by the user using the `flow` keyword. Flows are put together using functions of the standard library, however flows themselves are functions as well, capable of being reused for other flows.

## Defining a flow

When defining a flow, you must provide a name for the flow and the operations to be carried out by the flow itself. Both of these are mandatory for the flow to function. Your flows may not contain some special characters, most notably `@`, as that is used to separate namespaces from function names.

=== "Syntax"

    ``` lncf
    flow "your flow name" {
        flow components...
    }
    ```

=== "Example"

    ``` lncf
    flow "contains accent" {
        fn "lib@has match" {
            arg "1", "áéíóú"
        }
    }
    ```

    This flow checks whether a given string contains any of the accent characters áéíóú.

## Mutating and non-mutating flows

There are some restrictions on what may or may not be used in a flow depending on if the flow you're defining is intended to directly modify the word. Learn more about mutating functions here ==^Add\ link^==

### Mutating flows (`!`)

Mutating flows modify the current word being processed. These flows can call multiple mutating functions in a row, linearly. To mark a flow as mutating, simply name the flow with an exclamation mark `!` as the last character. Note that exclamation marks may not be used anywhere else but at the end of a flow.

??? example
    ``` lncf
    flow "mutating!" {
        fn "mutating function 1!",
        fn "mutating function 2!",
        fn "mutating function 3!"
    }
    ```

    The top level of a mutating flow can be made up of 1 or more mutating functions or control keywords

### Non-mutating flows

Non-mutating flows work more like typical functions, returning values for other flows or functions to use. As these flows don't have access to the state of the word, they can't read or modify it, nor use other mutating functions (`!`). Also, these flows may not call multiple functions in a row, being more recursive than linear in its syntax.

??? example
    ``` lncf
    flow "non-mutating" {
        fn "other function"
    }
    ```

    The top level of a non-mutating flow can be made up of only a single non-mutating function or a control keyword.


