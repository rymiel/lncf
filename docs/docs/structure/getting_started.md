# What's in a configuration file?

The top level of an `.lncf.lua` file consists of primarily [flows][2] and vectors, used to transform the words of the language.

Because every `.lncf.lua` file, as the extension suggests, is a fully-fledged [lua file][1], you may use a lot of lua's capabilities for the simplification of your configurations, such as reusing variables.

  [1]: https://lua.org/
  [2]: flows.md

!!! note
    The prohibited features for configuration files are for security of the cloud platform, namely interaction with the filesystem.

## LNCF keywords

The API of the LNCF system (`liblncf.lua`) is exposed as a set of global functions. These functions are usually overloaded to support parentheses-less syntax for invoking functions, allowing for simpler syntax for people who may have not used a language like lua before.  
Note that from now on these functions will be refered to as "keywords", despite not being strictly keywords of the lua language, these functions visually look like keywords and are crucial for configuration file operation.

!!! example
    ``` lncf
    flow "stress first!" {
        fn "lib@map characters!" {
            "aàeiouy", "ááéíóúý",
            limit=1
        }
    }
    ```

    Note that here `flow` and `fn` are a part of the aforementioned set of global functions or "keywords", discussed in detail later.
