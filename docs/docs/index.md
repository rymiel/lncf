# LNCF - LaNguage ConFig

## User guide

This site will be the source of information for actually composing your own `.lncf.lua` configuration file, whereas the repository wiki is meant more to talk about the implementation of the system.

!!! danger
    In the current state of the project, this page is actually more of an outline on which the rest of the system is designed by.
    Therefore it serves as the ideal destination of the project's backend and might change drastically in the future when implementation is started
